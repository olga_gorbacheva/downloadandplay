package ru.god.downloadandplay;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

/**
 * Класс, предназначенный для создания потоков для скачивания медиа-файлов из сети
 *
 * @author Горбачева, 16ИТ18к
 */
public class DownloadThread extends Thread {
    private URL url;
    private String file;

    DownloadThread(String url, String file) throws MalformedURLException {
        this.url = new URL(url);
        this.file = file;
    }

    /**
     * Метод, выполняющий скачивание файлов по указанному URL в указанный каталог
     */
    public void run() {
        try (ReadableByteChannel byteChannel = Channels.newChannel(url.openStream());
             FileOutputStream stream = new FileOutputStream(file)) {
            stream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
            System.out.println("Скачивание файла " + file + " прошло успешно");
        } catch (IOException e) {
            System.out.println("Произошла ошибка при скачивании файла " + file);
        }
    }
}
