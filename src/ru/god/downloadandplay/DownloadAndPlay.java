package ru.god.downloadandplay;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

/**
 * Класс, предназначенный для скачивания медиа-файлов из источника в сети
 * и воспроизведения файлов типа mp3
 *
 * @author Горбачева, 16ИТ18к
 */
public class DownloadAndPlay {
    private static ArrayList<String> musicTarget = new ArrayList<>();
    private static ArrayList<DownloadThread> musicFiles = new ArrayList<>();

    public static void main(String[] args) {
        try {
            List<String> list = sourceAndTargetInfo("D:\\work\\downloadmusic\\src.txt");
            for (String aList : list) {
                String[] array = aList.split(" ");
                DownloadThread downloadThread = new DownloadThread(array[0], array[1]);
                if (array[1].contains(".mp3")) {
                    musicTarget.add(array[1]);
                    musicFiles.add(downloadThread);
                }
                downloadThread.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        playAllMusic();
    }

    /**
     * Возвращает список строк, содержащих пару значений <ссылканафайл><пробел><путьдлясохранения>
     *
     * @param file - путь к файлу с исходными данными
     * @return список строк, содержащих пару значений <ссылканафайл><пробел><путьдлясохранения>
     * @throws IOException - ошибка ввода-вывода
     */
    private static List<String> sourceAndTargetInfo(String file) throws IOException {
        return Files.readAllLines(Paths.get(file));
    }

    /**
     * Метод, позволяющий воспроизвести отдельный музыкальный файл
     *
     * @param musicTarget - путь к месту хранения музыкального файла
     */
    private static void playMusic(String musicTarget) {
        try (FileInputStream inputStream = new FileInputStream(musicTarget)) {
            Player player = new Player(inputStream);
            player.play();
        } catch (IOException | JavaLayerException e) {
            e.printStackTrace();
        }
    }

    /**
     * Метод, позволяющий воспроизвести все музыкльные файлы
     */
    private static void playAllMusic() {
        for (DownloadThread thread : musicFiles) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        for (String music : musicTarget) {
            playMusic(music);
        }
    }
}
